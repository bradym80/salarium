/**
 * View Models used by Spring MVC REST controllers.
 */
package com.fsrin.salarium.web.rest.vm;
