(function() {
    'use strict';

    angular
        .module('salariumApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
